\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}icarogw package}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Subpackages}{3}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}icarogw.analyses package}{3}{subsection.1.1.1}%
\contentsline {subsubsection}{Submodules}{3}{subsubsection*.3}%
\contentsline {subsubsection}{icarogw.analyses.cosmo\_pop\_rate\_marginalized module}{3}{subsubsection*.4}%
\contentsline {subsubsection}{Module contents}{4}{subsubsection*.10}%
\contentsline {subsection}{\numberline {1.1.2}icarogw.priors package}{4}{subsection.1.1.2}%
\contentsline {subsubsection}{Submodules}{4}{subsubsection*.11}%
\contentsline {subsubsection}{icarogw.priors.custom\_bilby\_priors module}{4}{subsubsection*.12}%
\contentsline {subsubsection}{icarogw.priors.custom\_math\_priors module}{6}{subsubsection*.26}%
\contentsline {subsubsection}{icarogw.priors.mass module}{10}{subsubsection*.53}%
\contentsline {subsubsection}{icarogw.priors.redshift module}{11}{subsubsection*.58}%
\contentsline {subsubsection}{Module contents}{11}{subsubsection*.61}%
\contentsline {subsection}{\numberline {1.1.3}icarogw.utils package}{11}{subsection.1.1.3}%
\contentsline {subsubsection}{Submodules}{11}{subsubsection*.62}%
\contentsline {subsubsection}{icarogw.utils.conversions module}{11}{subsubsection*.63}%
\contentsline {subsubsection}{icarogw.utils.plotting module}{12}{subsubsection*.68}%
\contentsline {subsubsection}{icarogw.utils.quick\_init module}{13}{subsubsection*.71}%
\contentsline {subsubsection}{Module contents}{13}{subsubsection*.75}%
\contentsline {section}{\numberline {1.2}Submodules}{13}{section.1.2}%
\contentsline {section}{\numberline {1.3}icarogw.cosmologies module}{13}{section.1.3}%
\contentsline {section}{\numberline {1.4}icarogw.injections module}{15}{section.1.4}%
\contentsline {section}{\numberline {1.5}icarogw.posterior\_samples module}{17}{section.1.5}%
\contentsline {section}{\numberline {1.6}Module contents}{18}{section.1.6}%
\contentsline {chapter}{\numberline {2}Indices and tables}{19}{chapter.2}%
\contentsline {chapter}{Python Module Index}{21}{section*.110}%
\contentsline {chapter}{Index}{23}{section*.111}%
