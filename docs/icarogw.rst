icarogw package
===============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   icarogw.analyses
   icarogw.priors
   icarogw.utils

Submodules
----------

icarogw.cosmologies module
--------------------------

.. automodule:: icarogw.cosmologies
   :members:
   :undoc-members:
   :show-inheritance:

icarogw.injections module
-------------------------

.. automodule:: icarogw.injections
   :members:
   :undoc-members:
   :show-inheritance:

icarogw.posterior\_samples module
---------------------------------

.. automodule:: icarogw.posterior_samples
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: icarogw
   :members:
   :undoc-members:
   :show-inheritance:
