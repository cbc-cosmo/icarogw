Welcome to icarogw's documentation!
===================================
Welcome to the documentation of icarogw

.. toctree::
   :maxdepth: 3

   :caption: Contents:

   icarogw
   icarogw.analyses
   icarogw.priors
   icarogw.utils


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
